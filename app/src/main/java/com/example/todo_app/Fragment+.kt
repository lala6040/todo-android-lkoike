package com.example.todo_app

import android.app.AlertDialog
import androidx.fragment.app.Fragment

fun Fragment.showErrorDialog(message: String) {
    AlertDialog.Builder(requireContext())
        .setTitle(R.string.error_dialog_title)
        .setMessage(message)
        .setPositiveButton(R.string.error_dialog_close, null)
        .show()
}
