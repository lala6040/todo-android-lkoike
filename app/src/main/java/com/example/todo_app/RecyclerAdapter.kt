package com.example.todo_app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_text.view.*

class RecyclerAdapter(private val onClickTodo: (Todo) -> Unit) :
    RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder>() {

    var todoList: List<Todo> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.cell_text, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val todo = todoList[position]
        holder.itemView.text_title.text = todo.title
        holder.itemView.setOnClickListener { onClickTodo(todo) }
    }

    override fun getItemCount(): Int {
        return todoList.size
    }

    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
