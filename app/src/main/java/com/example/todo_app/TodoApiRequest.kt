package com.example.todo_app

import retrofit2.Call
import retrofit2.http.*

interface TodoApiRequest {
    @GET("todos")
    fun fetchTodo(): Call<TodosGetResponse>

    @POST("todos")
    fun createTodo(
        @Body todoRequestBody: TodoRequestBody
    ): Call<CommonResponse>

    @PUT("todos/{id}")
    fun updateTodo(
        @Path("id") id: Int,
        @Body todoRequestBody: TodoRequestBody
    ): Call<CommonResponse>

    @DELETE("todos/{id}")
    fun deleteTodo(
        @Path("id") id: Int
    ): Call<CommonResponse>
}
