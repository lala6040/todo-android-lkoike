package com.example.todo_app

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_todo_edit.*
import android.graphics.Color
import android.icu.text.SimpleDateFormat
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class TodoEditFragment : Fragment() {

    private val args: TodoEditFragmentArgs by navArgs()
    private val displayFormat = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
    private val sendingFormat = SimpleDateFormat(DATE_FORMAT_SERVER, Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_todo_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val todo = args.todo
        todo?.let {
            setUpTexts(it)
        }
        setUpButton(todo?.id)
        addTextChangedListener()
        text_date_content.setOnClickListener {
            showDatePickerDialog()
        }
        text_title_max.text = getString(R.string.text_max, MAX_TITLE_LENGTH)
        text_detail_max.text = getString(R.string.text_max, MAX_DETAIL_LENGTH)
    }

    private fun addTextChangedListener() {
        edit_text_title.addTextChangedListener {
            text_title_word_counter.text = edit_text_title.length().toString()
            changeTitleWordCounterTextColor()
            changeCompleteButtonState()
        }

        edit_text_detail.addTextChangedListener {
            text_detail_word_counter.text = edit_text_detail.length().toString()
            changeDetailWordCounterTextColor()
            changeCompleteButtonState()
        }
    }

    private fun changeTitleWordCounterTextColor() {
        if (edit_text_title.length() <= MAX_TITLE_LENGTH) {
            text_title_word_counter.setTextColor(Color.GRAY)
        } else {
            text_title_word_counter.setTextColor(Color.RED)
        }
    }

    private fun changeDetailWordCounterTextColor() {
        if (edit_text_detail.length() <= MAX_DETAIL_LENGTH) {
            text_detail_word_counter.setTextColor(Color.GRAY)
        } else {
            text_detail_word_counter.setTextColor(Color.RED)
        }
    }

    private fun changeCompleteButtonState() {
        val isTitleTextValid = edit_text_title.length() in 1..MAX_TITLE_LENGTH
        val isDetailTextValid = edit_text_detail.length() <= MAX_DETAIL_LENGTH
        button_edit_complete.isEnabled = isTitleTextValid && isDetailTextValid
    }

    private fun showDatePickerDialog() {
        val calender = Calendar.getInstance()
        val year = calender.get(Calendar.YEAR)
        val month = calender.get(Calendar.MONTH)
        val day = calender.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(
            requireContext(),
            DatePickerDialog.OnDateSetListener { _, y, m, d ->
                text_date_content.text = ("$y/${m + 1}/$d")
            },
            year,
            month,
            day
        )
        datePickerDialog.setButton(
            DialogInterface.BUTTON_NEUTRAL,
            getString(R.string.delete_button_date_picker)
        ) { _, _ ->
            text_date_content.text = ""
        }
        datePickerDialog.show()
    }

    private fun setUpTexts(todo: Todo) {
        edit_text_title.setText(todo.title)
        todo.detail?.let {
            edit_text_detail.setText(todo.detail)
        }
        todo.date?.let {
            text_date_content.text = displayFormat.format(todo.date)
        }
    }

    private fun setUpButton(id: Int?) {
        if (id != null) {
            button_edit_complete.text = getString(R.string.button_update)
        } else {
            button_edit_complete.text = getString(R.string.button_registration)
            button_edit_complete.isEnabled = false
        }
        button_edit_complete.setOnClickListener {
            CoroutineScope(Dispatchers.Default).launch {
                sendTodo(id)
            }
        }
    }

    private suspend fun sendTodo(id: Int?) {
        try {
            val request = TodoApiClient().apiRequest
            val response = if (id != null) {
                request.updateTodo(id, makeBody()).execute()
            } else {
                request.createTodo(makeBody()).execute()
            }
            withContext(Dispatchers.Main) {
                val body = if (response.isSuccessful) {
                    response.body()!!
                } else {
                    Gson().fromJson(response.errorBody()?.string(), CommonResponse::class.java)
                }
                onReceiveResponse(body, id != null)
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                showErrorDialog(getString(R.string.unknown_error_message))
            }
        }
    }

    private fun onReceiveResponse(response: CommonResponse, isUpdate: Boolean) {
        if (response.errorCode != 0) {
            showErrorDialog(response.errorMessage)
            return
        }
        val resourceID =
            if (isUpdate) R.string.message_todo_update else R.string.message_todo_registered
        onSuccess(getString(resourceID))
    }

    private fun onSuccess(message: String) {
        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.value = Event(message)
        findNavController().navigateUp()
    }

    private fun makeBody(): TodoRequestBody {
        val title = edit_text_title.text.toString()
        val detail = edit_text_detail?.text?.toString()
        val date =
            if (text_date_content.text.isBlank()) {
                ""
            } else {
                val parseDate = displayFormat.parse(text_date_content.text.toString())
                sendingFormat.format(parseDate)
            }
        return TodoRequestBody(title, detail, date)
    }

    companion object {
        private const val MAX_TITLE_LENGTH = 100
        private const val MAX_DETAIL_LENGTH = 1000
        private const val DATE_FORMAT = "yyyy/M/d"
        private const val DATE_FORMAT_SERVER = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
}
