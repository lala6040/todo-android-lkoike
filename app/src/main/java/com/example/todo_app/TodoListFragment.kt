package com.example.todo_app

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_todo_list.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TodoListFragment : Fragment() {

    private val adapter = RecyclerAdapter(::onClickTodoItem)
    private var isDeleteMode = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.observeEvent(this) {
            Snackbar.make(requireView(), it, Snackbar.LENGTH_LONG).show()
        }
        return inflater.inflate(R.layout.fragment_todo_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layout = LinearLayoutManager(activity)
        recycler_todo.also {
            it.layoutManager = LinearLayoutManager(activity)
            it.adapter = adapter
            it.addItemDecoration(DividerItemDecoration(activity, layout.orientation))
            it.setHasFixedSize(true)
        }
        CoroutineScope(Dispatchers.Default).launch {
            fetchTodoList()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_todo_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // 作成ボタンを押したとき
            R.id.menu_create -> {
                goToEditFragment()
                isDeleteMode = false
            }
            // 削除ボタンを押したとき
            R.id.menu_delete -> {
                isDeleteMode = !isDeleteMode
                val icon = if (isDeleteMode) R.drawable.ic_check else R.drawable.ic_delete_outline
                item.setIcon(icon)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private suspend fun fetchTodoList() {
        try {
            val response = TodoApiClient().apiRequest.fetchTodo().execute()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    adapter.todoList = response.body()?.todos!!
                } else {
                    val body = Gson().fromJson(response.errorBody()?.string(), CommonResponse::class.java)
                    showErrorDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                showErrorDialog(getString(R.string.unknown_error_message))
            }
        }
    }

    private suspend fun deleteTodo(id: Int) {
        try {
            val response = TodoApiClient().apiRequest.deleteTodo(id).execute()
            if (response.isSuccessful) {
                fetchTodoList()
            } else {
                val body = Gson().fromJson(response.errorBody()?.string(), CommonResponse::class.java)
                withContext(Dispatchers.Main) {
                    showErrorDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                showErrorDialog(getString(R.string.unknown_error_message))
            }
        }
    }

    private fun onClickTodoItem(todo: Todo) {
        if (isDeleteMode) {
            showDeleteConfirmationDialog(todo)
        } else {
            goToEditFragment(todo)
        }
    }

    private fun goToEditFragment(todo: Todo? = null) {
        val action = TodoListFragmentDirections.actionToEdit(todo)
        findNavController().navigate(action)
    }

    private fun showDeleteConfirmationDialog(todo: Todo) {
        AlertDialog.Builder(context)
            .setMessage(getString(R.string.message_delete_confirmation_dialog, todo.title))
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                CoroutineScope(Dispatchers.Default).launch(Dispatchers.IO) {
                    deleteTodo(todo.id)
                }
            }
            .show()
    }
}
